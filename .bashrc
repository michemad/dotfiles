#                     _   _               _
# _ __ ___   __ _  __| | | |__   __ _ ___| |__
#| '_ ` _ \ / _` |/ _` | | '_ \ / _` / __| '_ \
#| | | | | | (_| | (_| | | |_) | (_| \__ \ | | |
#|_| |_| |_|\__,_|\__,_| |_.__/ \__,_|___/_| |_|
#
#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#PS1='[\u@\h \W]\$ '
PS1="[\W] > "

export EDITOR=nvim
export VISUAL=nvim
export BROWSER=firefox
export TERMINAL=alacritty

export PATH="~/.scripts:$PATH"

#########################################################

# alias
alias ls='ls --color=auto'

alias q='exit'

alias vim='nvim'
alias vi='nvim'
alias v='nvim'

alias matlab='setsid -f matlab'
alias py='python -m IPython'

alias r='ranger'

alias cmatrix='cmatrix -abr'
alias cbonsai='cbonsai -li'

alias kz='killall zoom'

#########################################################

# fzf configuration
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash

export FZF_DEFAULT_COMMAND='fd . --hidden --follow'
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --info=inline --cycle --prompt="Search: " --color fg:#D8DEE9,bg:#000000,hl:#A3BE8C,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C,pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#81A1C1,marker:#EBCB8B'
export FZF_CTRL_T_COMMAND='$FZF_DEFAULT_COMMAND'

#########################################################

echo ""
neofetch
#bash .scripts/ufetch
