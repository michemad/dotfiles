## Arch Linux Installation (Dell XPS13)

#### Live USB

- `$ sudo fdisk -l` to check the device name (ex. /dev/sdb1)
- `$ umount /dev/sdb1` unmount the device
- `$ mkfs.vfat /dev/sdb1 -I` format to FAT
- `$ dd if=~/path/to/Arch.iso of=/dev/sdb1` write the iso on USB

#### Manual Installation

Boot into Live USB containing Arch Linux by pressing F12 repeatedly as soon as Dell Logo comes up, then from the menu select Arch Linux.

Things to do immidiately after booting into Live USB:
- `$ loadkeys it` change keyboard layout 
- delete old UEFI Boot entries:
	* `$ efibootmgr` list all entries (ex. Boot0001* mad)
	* `$ efibootmgr -b 0001 -B` remove the entry correspondent to Boot0001* mad
- Connect to internet:
	* `$ iwctl`
	* `$ [iwd]# station wlan0 get-networks` (ex. netis_7EB600)
	* `$ [iwd]# station wlan0 connect netis_7EB600` then insert password
	* `$ [iwd]# exit`
	* `$ ping archlinux.org` to check connection
	
	 **Note**: if the wireless interface is not up do the following:

	* `$ rfkill unblock all`
	* `$ ip link` and check the name of wireless interface (ex *wlan0*)
	* `$ ip link set wlan0 up`

- `$ sudo pacman -Syyu` to refresh mirrorlist
- Create partitions:
	* `$ lsblk` to check disks and partitions (suppose we want to install Arch on */dev/nvme0n1*)
	* `$ cfdisk /dev/nvme0n1` and use `cfdisk` interface to manage partitions:

| Partition | Size | Type | Mount Point | example |
|-|-|-|-|-|
| Boot | 500Mb | EFI system | /mnt/boot | /dev/nvme0n1p1 |
| Swap | half RAM | Linux swap | [SWAP] | /dev/nvme0n1p2 |
| Root | remaining | Linux filesystem | /mnt | /dev/nvme0n1p3 |

- Format partitions:
```
$ mkfs.fat -F32 /dev/nvme0n1p1
$ mkswap /dev/nvme0n1p2
$ swapon
$ mkfs.ext4 /dev/nvme0n1p3
```

- Mount partitions:
```
$ mount /dev/nvme0n1p3 /mnt
$ mkdir /mnt/boot
$ mount /dev/nvme0n1p1 /mnt/boot
```

- Base installation:
```
$ pacstrap /mnt base base-devel linux-lts linux-lts-headers linux-firmware sudo efibootmgr dosfstools os-prober mtools networkmanager man-db man-pages intel-ucode xf86-video-intel vim git
```

- Fstab:
```
$ genfstab -U /mnt >> /mnt/etc/fstab
```

- Enter the installation:
```
$ arch-chroot /mnt
```

- Date and time:
```
$ ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime
$ hwclock --systohc
```

- Generating locales: edit `/etc/locale.gen` and uncomment the line
```
#en_US.UTF-8 UTF-8
```
Then  run
```
$ locale-gen
```

- Set language and keyboard layout: edit `/etc/locale.conf` by adding
```
LANG=en_US.UTF-8
LANGUAGE=en_US
LC_ALL=en_US.UTF-8
```
Then edit `/etc/vconsole.conf` by adding
```
KEYMAP=it
```

- Hostname and user: edit `/etc/hostname` by adding hostname ("mad" in this example)
```
mad
```
Then edit `/etc/hosts` by adding
```
127.0.0.1	localhost
::1		localhost
127.0.1.1	mad.localdomain		mad
```
Type
```
$ passwd
```
and insert the root password.
Now we set the main user ("miche" in this example) with the command
```
$ useradd -m -G wheel miche
```
Then
```
$ passwd miche
```
and type a password for the new user.
Finally we get rid of some annoying permissions requests by editing the `visudo` file: type 
```
$ EDITOR=vim visudo
```
to edit the file and uncomment
```
# %wheel ALL=(ALL) ALL
# %wheel ALL=(ALL) NOPASSWD:ALL
```

- Bootloader (GRUB):
```
$ pacman -S grub
$ grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=ArchLinux
$ grub-mkconfig -o /boot/grub/grub.cfg
```
**Note**: (only for expert users) if you don't want grub you can use `efibootmgr` to load the kernel directly. For this I suggest to read the script and for Dell XPS use `arch-efiboot` (see last three steps in the script installation guide).

- Wifi (we need to configure the network now, otherwise we won't be able to install any package on our fresh Arch Linux) 
```
$ pacman -S networkmanager
$ systemctl start NetworkManager
$ systemctl enable NetworkManager
```
Now run
```
$ nmtui
```
and set up the connection.

- Conclusion:
```
$ exit
$ umount -a
$ reboot
```
Unplug the USB and wait for the system to reboot into Arch Linux.


#### Script Installation 

Boot into Live USB containing Arch Linux by pressing F12 repeatedly as soon as Dell Logo comes up, then from the menu select Arch Linux.

Things to do immidiately after booting into Live USB:
- `$ loadkeys it` change keyboard layout 
- delete old UEFI Boot entries:
	* `$ efibootmgr` list all entries (ex. Boot0001* mad)
	* `$ efibootmgr -b 0001 -B` remove the entry correspondent to Boot0001* mad
- Connect to internet:
	* `$ iwctl`
	* `$ [iwd]# station wlan 0 get-networks` (ex. netis_7EB600)
	* `$ [iwd]# station wlan0 connect netis_7EB600`
	* `$ [iwd]# exit`
	* `$ ping archlinux.org` to check connection

- Cloning the installation script and the dotfiles:
	* `$ sudo pacman -Syyy` refresh package databases
	* `$ sudo pacman -S git vim` download Git and Vim
	* `$ git clone https://gitlab.com/michemad/dotfiles.git`
- Now go into `dotfiles/.scripts/archinstall` and edit the `cfg` file in order to edit disk names, add username, hostname and password, add or remove packages. Then execute `arch` and wait until the installation is complete.
- *there are some issues with the bootloader, so we have to proceed in this way:*
**without unplugging the USB** `reboot` the system, press F12 in order to boot from the USB and from the menu select the EFI Shell.
*Important*: don't press any key in order to let the shell execute automatically the `startup.sh` script, that will boot the newly installed Arch-Linux from the disk.
- Now Unplug the USB and do the following:
	* `$ sudo systemctl enable NetworkManager`
	* `$ sudo systemctl start NetworkManager`
	* launch `nmtui` and connect to internet
	* `$ yay -S arch-efiboot` (we need this magical package)
- Then `reboot` and go straight to the Bios (pressing F2 instead of F12 like before). Navigate to *General -> Boot Sequence -> Add Boot Option*. From there select `linux-lts.efi` (this file is created by `arch-efiboot` previously download), put it on top of the boot sequence, click *Apply* and exit.

Now the computer should boot directly into arch-linux, the installation is complete!

## Setup
#### Bluetooth
Enable bluetooth by running 
```
$ sudo systemctl enable bluetooth.service
```

**Note:** fix AirPods low volume by editing `bluetooth.service`:

```
$ sudo systemctl edit --full bluetooth.service
``` 
and change the line
```
Exec Start=/usr/lib/bluetooth/bluetoothd
```
to
```
Exec Start=/usr/lib/bluetooth/bluetoothd --noplugin=avrcp
```
Finally restart and reload:

* `$ sudo systemctl restart bluetooth.service`
* `$ sudo systemctl daemon-reload`

#### /home and dotfiles

Create all folders 
```
$ mkdir Documents Downloads Pictures Trash Videos Music Dropbox GoogleDrive
```
and clone dotfiles 
```
$ git clone https://gitlab.com/michemad/dotfiles.git
```

Create a link for every element of the dotfiles folder into the home directory, for example `$ sudo ln -sf ~/dotfiles/.config ~/` (**Note:** it may be necessary to delete the default `.config` folder from home directory first).

Compile dwm, dmenu, dwmblocks, st by running in each respective folder
```
$ sudo rm config.h
$ sudo make clean install
```
**Note:** use `blocks.h` instead of `config.h` in dwmblocks.

Make the scripts executable by running `$ sudo chmod +x ~/.scripts/*`.

Finally run 
```
$ sudo ln -sf /run/media/miche/ /home/MEDIA
```
to create a folder linked to where all external drives are mounted automatically by udiskie.

#### vim Plug

To download vim Plug run
```
$ curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
Then open `nvim` and type `:PlugInstall`.

#### Keyboard layout

Sometimes the installation makes a mess with the keyboard layout, so run 
```
$ sudo localectl set-x11-keymap it
```
then edit `/etc/vconsole.conf` by adding
```
KEYMAP=it
```

#### Touchpad

Edit `/usr/share/X11/xorg.conf.d/40-libinput.conf` by adding under the section about touchpad
```
Option "Tapping" "on"
Option "ClickMethod" "clickfinger"
Option "NaturalScrolling" "true"
Option "AccelSpeed" "0.5"
```

#### Prompt only the password for default user in virtual console login

```
$ sudo systemctl edit getty@tty1
```
Insert the following:
```
[Service]
ExecStart=
ExecStart=-/sbin/agetty -n -o miche %I
```
where *miche* is my username. Then run 
```
$ sudo systemctl enable getty@tty1
```

#### rclone

First, create two remotes *dropbox* and *drive*, then 
```
$ mkdir ~/GoogleDrive/Universita
```
and execute `syncmynotes` in order to syncronize all my university notes.

#### Matlab

Download the .zip file from MathWorks, then move it to a newly created folder called *Matlab* and unzip. Now run the installation script `$ sudo bash install` (**Note:** it's important to use *sudo*). Proceed with the installation, make sure to use the same login name used for linux (*miche* in my case) and to check the symlink option.

We need to install `libselinux` package from AUR.

For some window managers we also need to put in our *.xinitrc* file
```
# export _JAVA_AWT_WM_NONREPARENTING=1
```

Now for tweaking scaling and fonts do the following:
- open matlab an type
```
>> s = settings;s.matlab.desktop.DisplayScaleFactor
>> s.matlab.desktop.DisplayScaleFactor.PersonalValue = 1.5
```
- go to *Preferences -> fonts* and check the anti-aliasing option

The settings take effect after matlab is restarted.

To add toolboxes and add-ons we need to change some permissions:
```
$ sudo chown -R miche /usr/local/MATLAB/version
```
where *miche* is my username.

Remember:
- `$ setsid -f matlab` to open a new process, so you can close the terminal window after running matlab
- `$ matlab -nodisplay` for a cli-version

To uninstall Matlab do the following:
```
$ sudo rm /var/tmp/lm_TMW*
$ sudo rm -rf /usr/local/MATLAB/R2020b
$ sudo rm /usr/local/bin/matlab /usr/local/bin/mcc /usr/local/bin/mex /usr/local/bin/mbuild
```

