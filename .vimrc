"                     _         _
" _ __ ___   __ _  __| | __   _(_)_ __ ___
"| '_ ` _ \ / _` |/ _` | \ \ / / | '_ ` _ \
"| | | | | | (_| | (_| |  \ V /| | | | | | |
"|_| |_| |_|\__,_|\__,_|   \_/ |_|_| |_| |_|
"
"
" PLUGIN -------------------------------------------------------------------

call plug#begin('~/.vim/plugged')

Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'arcticicestudio/nord-vim'
Plug 'itchyny/lightline.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

call plug#end()

" COLORS -------------------------------------------------------------------

let g:Hexokinase_highlighters = ['backgroundfull']

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (guicolors option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#2160511 >
  if (has("termguicolors"))
     set termguicolors
  endif
endif

augroup nord-theme-overrides
  autocmd!
  " Use 'nord7' as foreground color for Vim comment titles.
  autocmd ColorScheme nord highlight Normal ctermbg=0 guibg=#000000
augroup END

colorscheme nord

" SETTINGS -----------------------------------------------------------------

set mouse=a
"set cursorline
set backspace=indent,eol,start
set number
set relativenumber
set showcmd
set splitbelow splitright
set ignorecase incsearch smartcase
"set hlsearch 
set lazyredraw
set ttyfast

set laststatus=2 "for lightline

" CLIPBOARD ----------------------------------------------------------------

set clipboard=unnamedplus
:inoremap <C-p> <ESC>"+gP
:nnoremap <C-p> <ESC>"+gP
:vnoremap <C-c> "+y
:vnoremap <C-x> "+d
