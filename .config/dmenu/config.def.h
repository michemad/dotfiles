/*                     _       _
 _ __ ___   __ _  __| |   __| |_ __ ___   ___ _ __  _   _
| '_ ` _ \ / _` |/ _` |  / _` | '_ ` _ \ / _ \ '_ \| | | |
| | | | | | (_| | (_| | | (_| | | | | | |  __/ | | | |_| |
|_| |_| |_|\__,_|\__,_|  \__,_|_| |_| |_|\___|_| |_|\__,_|

*/

/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */
/* -fn option overrides fonts[0]; default X11 font or font set */

static int topbar 		= 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy 		= 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered 		= 0;                    /* -c option; centers dmenu on screen */
static int min_width 		= 500;                    /* minimum width when centered */
static const unsigned int alpha = 180;
static const char *fonts[] 	= { "Inconsolata Nerd Font:style:Bold:size=12" };
static const char *prompt      	= NULL;      /* -p  option; prompt to the left of input field */

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;

static const unsigned int alphas[SchemeLast][2] = {
	 /*     		    fg         bg       */
        [SchemeNorm] 		= { OPAQUE, alpha },
        [SchemeSel]  		= { OPAQUE, alpha },
        [SchemeSelHighlight]  	= { OPAQUE, alpha },
        [SchemeNormHighlight]  	= { OPAQUE, alpha },
        [SchemeOut]  		= { OPAQUE, alpha },
};

// GRUVBOX DARK COLORSCHEME
//static const char *colors[SchemeLast][2] = {
//	/*     				fg         bg       */
//	[SchemeNorm] 		= { "#bbbbbb", "#282828" },
//	[SchemeSel] 		= { "#ffffff", "#665c54" },
//	[SchemeSelHighlight] 	= { "#89b482", "#665c54" },
//	[SchemeNormHighlight] 	= { "#89b482", "#282828" },
//	[SchemeOut] 		= { "#282828", "#d3869b" },
//};

// NORD COLORSCHEME
static const char *colors[SchemeLast][2] = {
	 /*     			fg         bg       */
	[SchemeNorm] 		= { "#bbbbbb", "#000000" },
	[SchemeSel] 		= { "#ffffff", "#212121" },
	[SchemeSelHighlight] 	= { "#81a1c1", "#212121" },
	[SchemeNormHighlight] 	= { "#81a1c1", "#000000" },
	[SchemeOut] 		= { "#000000", "#bf616a" },
};

