/*                     _       _
 _ __ ___   __ _  __| |   __| |_      ___ __ ___
| '_ ` _ \ / _` |/ _` |  / _` \ \ /\ / / '_ ` _ \
| | | | | | (_| | (_| | | (_| |\ V  V /| | | | | |
|_| |_| |_|\__,_|\__,_|  \__,_| \_/\_/ |_| |_| |_|

*/

/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx    = 0;        /* border pixel of windows */
static const unsigned int snap        = 32;       /* snap pixel */
static const int swallowfloating      = 1;        /* 1 means swallow floating windows by default */
static const unsigned int gappih      = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv      = 10;       /* vert inner gap between windows */
static const unsigned int gappoh      = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov      = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps            = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar              = 1;        /* 0 means no bar */
static const int topbar               = 1;        /* 0 means bottom bar */
static const unsigned int baralpha    = 180;
static const unsigned int borderalpha = OPAQUE;
static const char *fonts[]            = { "Inconsolata Nerd Font:style:Bold:size=12" };
static const char dmenufont[]         = { "Inconsolata Nerd Font:style:Bold:size=12" };
static char prompt[] 		      = ">_: "; // prompt for dmenu

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */


static const unsigned int alphas[][3]      = {
	/*               	fg      bg        border     */
	[SchemeNorm] 	 = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  	 = { OPAQUE, baralpha, borderalpha },
	[SchemeStatus]   = { OPAQUE, baralpha, borderalpha },
	[SchemeTagsSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeTagsNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeInfoSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeInfoNorm] = { OPAQUE, baralpha, borderalpha },
};

// GRUVBOX DARK COLORSCHEME
//static const char col_bg[]          = "#282828";
//static const char col_gray2[]       = "#665c54";
//static const char col_gray3[]       = "#928374";
//static const char col_fg[]          = "#dfbf8e";
//static const char col_cyan[]        = "#89b482";
//static const char *colors[][3]      = {
//	/*               	fg       bg         border   */
//	[SchemeNorm] 	 = { col_gray3, col_bg,    col_gray2 },
//	[SchemeSel]      = { col_fg,    "#424242", "#ffffff" },
//	[SchemeStatus]   = { col_gray3, col_bg,    "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
//	[SchemeTagsSel]  = { "#ffffff", col_cyan,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
//      [SchemeTagsNorm] = { "#ffffff", col_gray2, "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
//      [SchemeInfoSel]  = { col_fg,    col_bg,    "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
//      [SchemeInfoNorm] = { col_gray3, col_bg,    "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
//};

// GRUVBOX DARK COLORSCHEME (DMENU)
//static char dmenusb[] = "#000000"; // background color for dmenu
//static const char *dmenucmd[] = { "dmenu_run", "-c", "-bw", "2", "-l", "10", "-m", dmenumon, "-p", prompt, "-fn", dmenufont, "-nb", col_bg, "-nf", "#bbbbbb", "-sb", dmenusb, "-sf", "#ffffff", NULL };

// NORD COLORSCHEME
static const char col_bg[]       = "#000000";
static const char col_gray2[]       = "#5e5e5e";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#ffffff";
static const char col_cyan[]        = "#5e81ac";
static const char *colors[][3]      = {
	/*               	fg         bg        border   */
	[SchemeNorm] 	 = { col_gray3, col_bg,    col_gray2 },
	[SchemeSel]  	 = { col_gray4, col_bg,    col_gray4 },
	[SchemeStatus]   = { col_gray3, col_bg,    col_bg  }, // Statusbar right
	[SchemeTagsSel]  = { col_gray4, col_cyan,  col_bg  }, // Tagbar left selected
        [SchemeTagsNorm] = { col_gray4, col_gray2, col_bg  }, // Tagbar left unselected
        [SchemeInfoSel]  = { col_gray4, col_bg,    col_bg  }, // infobar middle selected
        [SchemeInfoNorm] = { col_gray4, col_bg,    col_bg  }, // infobar middle unselected
};

// NORD COLORSCHEME (DMENU)
static char dmenusb[] = "#212121"; // background color for dmenu
static const char *dmenucmd[] = { "dmenu_run", "-c", "-bw", "2", "-l", "10", "-m", dmenumon, "-p", prompt, "-fn", dmenufont, "-nb", col_bg, "-nf", "#bbbbbb", "-sb", dmenusb, "-sf", "#ffffff", NULL };

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     	instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    	NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Zoom", 	NULL,     NULL,           1 << 8,    1,          0,          -1,        -1 },
	{ "Alacritty",  NULL,     NULL,           0,         0,          0,          1,        -1 },
	{ NULL,      	NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[F]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#include <X11/XF86keysym.h>

#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

static Key keys[] = {
	/* modifier                     key        function        argument */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	// GAPS (almost useless)
	{ MODKEY|ControlMask,  		XK_h,      incrgaps,       {.i = +3 } },
	{ MODKEY|ControlMask,  		XK_l,      incrgaps,       {.i = -3 } },
	{ MODKEY|ControlMask,  		XK_0,      togglegaps,     {0} },
	{ MODKEY|ControlMask,  		XK_space,  defaultgaps,    {0} },

	// DWM (modified)
	{ MODKEY|ControlMask,           XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_space,  zoom,           {0} }, // focused window becomes master
	{ MODKEY, 	                XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_r,      quit,           {0} }, // reload dwm

	// DWM (almost default)
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ControlMask,           XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ControlMask,           XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ControlMask,           XK_m,      setlayout,      {.v = &layouts[2]} },
//	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

	// PERSONAL
	{ MODKEY,	                XK_Return, spawn,          SHCMD("alacritty") },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          SHCMD("alacritty -e ranger") },
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_b,      spawn,          SHCMD("killall dwmblocks; setsid dwmblocks") },
	{ MODKEY,             		XK_s,      spawn,          SHCMD("~/.scripts/dmenu_websearch") },
	{ MODKEY|ShiftMask,             XK_v,      spawn,          SHCMD("~/.scripts/dmenu_videooutput; pkill -RTMIN+1 dwmblocks") },
	{ MODKEY|ShiftMask,             XK_a,      spawn,          SHCMD("~/.scripts/dmenu_audiooutput; pkill -RTMIN+1 dwmblocks") },
	{ MODKEY|ShiftMask,             XK_q,      spawn,          SHCMD("~/.scripts/dmenu_quitsession") },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          SHCMD("scrot ~/Pictures/%Y-%m-%d-%T-screenshot.png") },

	{ 0, 	XF86XK_AudioMute, 		   spawn, 	   SHCMD("pactl set-sink-mute @DEFAULT_SINK@ toggle; pkill -RTMIN+1 dwmblocks") },
	{ 0, 	XF86XK_AudioRaiseVolume,   	   spawn, 	   SHCMD("pactl set-sink-volume @DEFAULT_SINK@ +5%; pkill -RTMIN+1 dwmblocks") },
	{ 0, 	XF86XK_AudioLowerVolume,   	   spawn, 	   SHCMD("pactl set-sink-volume @DEFAULT_SINK@ -5%; pkill -RTMIN+1 dwmblocks") },
	{ 0, 	XF86XK_MonBrightnessUp,		   spawn, 	   SHCMD("xbacklight -inc 10; pkill -RTMIN+2 dwmblocks") },
	{ 0, 	XF86XK_MonBrightnessDown,	   spawn, 	   SHCMD("xbacklight -dec 10; pkill -RTMIN+2 dwmblocks") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

