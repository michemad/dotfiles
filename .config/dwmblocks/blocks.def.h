/*                   _       _                    _     _            _
 _ __ ___   __ _  __| |   __| |_      ___ __ ___ | |__ | | ___   ___| | _____
| '_ ` _ \ / _` |/ _` |  / _` \ \ /\ / / '_ ` _ \| '_ \| |/ _ \ / __| |/ / __|
| | | | | | (_| | (_| | | (_| |\ V  V /| | | | | | |_) | | (_) | (__|   <\__ \
|_| |_| |_|\__,_|\__,_|  \__,_| \_/\_/ |_| |_| |_|_.__/|_|\___/ \___|_|\_\___/

*/

//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/			/*Update Interval*/	/*Update Signal*/
	{" ", 		"bash ~/.scripts/dwmb_audio", 				5,		1},
	{"", 		"bash ~/.scripts/dwmb_brightness", 			0,		2},
	{"", 		"bash ~/.scripts/dwmb_bluetooth", 			5,		3},
	{"", 		"bash ~/.scripts/dwmb_network", 			5,		4},
	{"", 		"bash ~/.scripts/dwmb_battery", 			5,		5},
	{"", 		"date '+(%a) %d %b %Y - %H:%M '",			30,		6},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 3;
